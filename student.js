// import express
const express = require('express')

// import the database connectivity function
const dbmain = require('./dbmain')

const router = express.Router()

//============================================================
//1.add student
//============================================================

router.post('/student', (request, response) => {
    const { roll_no , name , class_ , division, dateofbirth , parent_mobile_no} = request.body
  
    
      const statement = `
        INSERT INTO student
            ( roll_no , name , class_ , division, dateofbirth , parent_mobile_no)
            VALUES( '${roll_no}', '${name}', '${class_}','${division}','${dateofbirth}','${parent_mobile_no}');
      `
    
      const connection = dbmain.connect()
      connection.query(statement, (error, result) => {
        connection.end()
    
        if (error) {
          response.send(error)
        } else {
          response.send(result)
        }
      })
    })
    //======================================================================
// 2. find student by roll no
//======================================================================
router.get('/student/roll_no/:roll_no', (request, response) => {
    
    const { roll_no } = request.params
    // statement to get all the student
    const statement = `select * from student where roll_no=${roll_no};`
  
    // get the connection
    const connection = dbmain.connect()
  
    // execute the query
    connection.query (statement, (error, students) => {
      // close the connection
  
      if (error) {
        response.send(error)
      } else {
        response.send(students)
    }
    })
  })
  
  
//======================================================================
// 3. Edit specific Student's class and division
//======================================================================


router.put('/student/roll_no/:roll_no', (request, response) => {
    const { roll_no} = request.params
    const {  class_ , division} = request.body
  
    const statement = `
      UPDATE student
      SET 
      class_ ='${class_}',division='${division}'
      WHERE roll_no= ${roll_no};
    `
  
    const connection = dbmain.connect()
    connection.query(statement, (error, result) => {
      connection.end()
  
      if (error) {
        response.send(error)
      } else {
        response.send(result)
      }
    })
  })
  
//====================================================================
// 4. delete student by roll_no
//====================================================================

router.delete('/student/roll_no/:roll_no', (request, response) => {
    const { roll_no } = request.params
  
    const statement = `
        DELETE FROM student
        WHERE roll_no = ${roll_no};
      `
  
    const connection = dbmain.connect()
    connection.query(statement, (error, result) => {
      connection.end()
  
      if (error) {
        response.send(error)
      } else {
        response.send(result)
      }
    })
  })
  
//=======================================================================
// 5. Fetch all students of particular class
//=======================================================================

router.get('/student/class/:class_', (request, response) => {
    
    const { class_ } = request.params
    // statement to get all the student
    const statement = `select * from student where class_=${class_};`
  
    // get the connection
    const connection = dbmain.connect()
  
    // execute the query
    connection.query (statement, (error, students) => {
      // close the connection
      connection.end()
  
      if (error) {
        response.send(error)
      } else {
        response.send(students)
    }
    })
  })
  

//==================================================================
// 6. Fetch all students of particular birth year
// ===================================================================
router.get('/student/year/:year', (request, response) => {
    const { year } = request.params
  
    // statement to get all the student
    const statement = `select * from student where year(dateofbirth)=${year};`
    
  
    // get the connection
    const connection = dbmain.connect()
  
    // execute the query
    connection.query (statement, (error, students) => {
      // close the connection
         connection.end()
         
      if (error) {
        response.send(error)
      } else {
        response.send(students)
    }
    })
  })

  
//=======================================================================

module.exports = router




