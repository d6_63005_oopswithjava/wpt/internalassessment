const mysql = require('mysql')

function connect()
{
//create connection to the database

    const connection = mysql.createConnection(
     {
       host : 'localhost',
       user: 'root',
       password: 'gauravgodge',
       database: 'student_db',
       port: 3306
     }
    )

      connection.connect()

      return connection
}

module.exports = {
    connect : connect,
      
}